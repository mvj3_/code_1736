public class EncrypDES {
	//
	private SecretKeyFactory keyFactory;
	private DESKeySpec desKeySpec;
	// SecretKey 负责保存对称密钥
	private SecretKey deskey;
	private String sKey = "12345678";
	// Cipher负责完成加密或解密工作
	private Cipher c;
	// 该字节数组负责保存加密的结果
	private String cipherStr;
	private static EncrypAES instance = null;

	public static EncrypAES getInstance() {
		if (instance == null) { // line 12

			try {
				instance = new EncrypAES();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return instance;
	}

	public EncrypAES() throws Exception {
		// 实例化支持DES算法的密钥生成器(算法名称命名需按规定，否则抛出异常)
		desKeySpec = new DESKeySpec(sKey.getBytes());
		keyFactory = SecretKeyFactory.getInstance("DES");
		deskey = keyFactory.generateSecret(desKeySpec);
		// 生成Cipher对象,指定其支持的DES算法
		c = Cipher.getInstance("DES");
	}

	/**
	 * 对字符串加密
	 * 
	 * @param str
	 * @return
	 * @throws InvalidKeyException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 */
	public String Encrytor(String str) throws InvalidKeyException,
			IllegalBlockSizeException, BadPaddingException {
		// 根据密钥，对Cipher对象进行初始化，ENCRYPT_MODE表示加密模式
		c.init(Cipher.ENCRYPT_MODE, deskey);
		// byte[] src = str.getBytes();
		// // 加密，结果保存进cipherByte
		// cipherByte = c.doFinal(src);
		//
		// return cipherByte;
		byte[] src = str.getBytes();
//		System.out.println("Encrytor------->" + new String(src));
		// 加密，结果保存进cipherByte
		byte[] cipherByte = c.doFinal(src);
//		System.out.println("Encrytor2------->" + new String(cipherByte));
		cipherStr = Base64.encodeToString(cipherByte, Base64.DEFAULT);
//		System.out.println("cipherStr------->" + cipherStr);
		return cipherStr;
	}

	/**
	 * 对字符串解密
	 * 
	 * @param buff
	 * @return
	 * @throws InvalidKeyException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 */
	public String Decryptor(String str) throws InvalidKeyException,
			IllegalBlockSizeException, BadPaddingException {
		// 根据密钥，对Cipher对象进行初始化，DECRYPT_MODE表示加密模式
		c.init(Cipher.DECRYPT_MODE, deskey);
		// cipherByte = c.doFinal(buff);
		// return cipherByte;
		byte[] buff = Base64.decode(str, Base64.DEFAULT);
		byte[] cipherByte = c.doFinal(buff);
		cipherStr = new String(cipherByte);
		return cipherStr;
	}

}
